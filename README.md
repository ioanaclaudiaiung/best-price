Numele aplicatiei este: **BEST PRICE**.
    
Monitorizarea unor site-uri de tipul e-commerce si 
notificarea userului printr-o alerta pe e-mail cand scade pretul unui produs dorit,
pana la valoarea asteptata.


**HOW IT'S WORK?**

Un client care are cont pe aplicatie si este logat va intra in pagina personala 
**View Products**. Aceasta contine o lista cu produsele sale de pe alte siteuri
pe care doreste sa le cumpere la un pret mai mic.
Cu ajutorul butonului ADD va introduce in formular urmatoarele date:
 - numele produsului
 - link-ul site-ul e-commerce unde a gasit produsul
 - url-ul site-ului e-commerce
 - pretul dorit 
 - optiunea de activare sau dezactivare a cautarii produsului

In momentul in care pretul produsuluiu va scadea pana la valoarea dorita, userul va primii o 
alerta pe e-mail.

**PAGINILE APLICATIEI:**

 **1. Home page**

Pe prima pagina va exista un paragraf care va descrie utilitatea aplicatiei.

Prima pagina va contine butoane pentru:
* Crearea unui cont de catre un utilizator nou
* Logarea utilizatorului

 **2. Create account**

Pe acesata pagina va exista un fromular pentru crearea unui cont de catre un utilizaror nou.

Campurile formularului vor fi:
* email
* passowrd
* repeat password
* sign in

 **3. Login**

Pe acesata pagina va exista un fromular pentru logarea utilizatorului.
* email
* password
* login

 **4. View products**

Pe acesata pagina va exista un fromular tip tabel unde sunt afisate produsele alese de utilizator
* id-ul produsului
* numele produsului
* pretul actual afisat pe site-ul e-commerce
* pretul dorit de catre utilizator
  

* buton pentru a sterge un produs
* buton pentru a adauga un produs
* buton editare produs

 **5. Add product**

Pe acesta pagina utilizatorul ajunge prin butonul: **Add** din pagina View products.

Pagina contine un formular cu datele produsului cautat de catre utilizator
* numele produsului
* numele site-ului e-commerce( va fi un camp drop-down)
* url-ul site-ului e-commerce
* pretul actual
* pretul dorit
* optiunea de a mentine activa cautarea sau de a dezactiva cautarea produsului
* buton ptr salvarea formularului


 


 



