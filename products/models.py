from django.contrib.auth.models import AbstractUser
from django.db import models
from .utils import get_link_data


class User(AbstractUser):
    pass


class Product(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE, related_name="user")
    name = models.CharField(max_length=300, null=True, blank=True)
    url = models.URLField()
    desired_price = models.FloatField(null=True, blank=True)
    web_price = models.FloatField(null=True, blank=True)
    updatedAt = models.DateTimeField(auto_now=True)
    createdAt = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        ordering = ('-createdAt',)

    def save(self, *args, **kwargs):
        name, price = get_link_data(self.url)
        self.name = name
        self.web_price = price

        super().save(*args, **kwargs)
