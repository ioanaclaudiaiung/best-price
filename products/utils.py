import requests
from bs4 import BeautifulSoup
import lxml


def get_link_data(url):
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML,"
                      " like Gecko) Chrome/89.0.4389.82 Safari/537.36",
        "Accept-Language": "en" "ro"
    }

    response = requests.get(url, headers=headers)

    soup = BeautifulSoup(response.text, "lxml")

    name = soup.select_one(selector=".page-title").getText()
    name = name.strip()

    price = soup.select_one(selector=".product-new-price").getText()
    price = price.strip()
    price = float(price[:-6])
    return name, price
