from django import forms
from django.contrib.auth.models import User
from products.models import Product
# from bootstrap_modal_forms.forms import BSModalModelForm


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username',
                  'email',
                  ]


class AddProdForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['url', 'desired_price', 'status']

# class AddBoolForm(forms.Form):
#     status = forms.BooleanField()

# class ProdBoosForm(BSModalModelForm):
#     class Meta:
#         model = Product
#         fields
