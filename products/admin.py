from django.contrib import admin
from products.models import *


class Usr(admin.ModelAdmin):
    list_display = ('id', 'username', 'email')


class Prod(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'name', 'desired_price', 'web_price', 'status')


admin.site.register(User, Usr)
admin.site.register(Product, Prod)
