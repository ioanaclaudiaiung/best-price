from django.urls import path, include
from . import views


urlpatterns = [
    path('', views.home_page_view, name='home'),

    # User
    path('logout', views.logout_view, name='logout'),
    path('login', views.login_view, name='login'),
    path('register', views.register, name='register'),
    path('account/', views.account, name='account'),
    path('account/edit', views.edit_account, name='edit_account'),
    path('account/change_password', views.change_password, name='change_password'),
    path('account/delete_account', views.delete_account, name='delete_account'),

    # Products
    path('create_item/', views.create_item, name='create_item'),
    path('create_item/edit_item/<int:pk>', views.ItemUpdateViewNoUrl.as_view(), name='edit_item_view'),
    path("delete_item/<int:pk>/", views.ItemDeleteView.as_view(), name='delete_item'),

]
