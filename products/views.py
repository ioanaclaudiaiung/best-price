from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import DeleteView, UpdateView
from products.form import AddProdForm
from products.models import User, Product
from products.utils import get_link_data


def home_page_view(request):
    return render(request, "home.html")


def login_view(request):
    # Check if the user is logged in
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse("home"))
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        # if user authentication success
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse("home"))
        else:
            return render(
                request,
                "registration/login.html",
                {"message": "Invalid username and/or password"},
            )
    return render(request, "registration/login.html")


def register(request):
    # Check if the user is logged in
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse("home"))
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        email = request.POST["email"]
        confirmation = request.POST["confirmation"]
        # Check if the password is the same as confirmation
        if password != confirmation:
            return render(request, "register.html", {"message": "Password must match"})
        # Check if the username is already in use
        try:
            user = User.objects.create_user(
                username=username, password=password, email=email
            )
            user.save()
            login(request, user)
            return HttpResponseRedirect(reverse("home"))
        except IntegrityError:
            return render(
                request, "register.html", {"message": "Username already taken"}
            )
    return render(request, "register.html")


def logout_view(request):
    # Logout the user
    logout(request)
    return HttpResponseRedirect(reverse("home"))


def account(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    return render(request, "account.html")


def edit_account(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    if request.method == "POST":
        user = User.objects.get(pk=request.user.pk)
        # edit the user's username and email
        user.username = request.POST["username"]
        user.email = request.POST["email"]
        user.save()
        login(request, user)
        return HttpResponseRedirect(reverse("account"))
    return render(request, "edit_account.html")


def change_password(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    if request.method == "POST":
        user = User.objects.get(pk=request.user.pk)
        old_password = request.POST["old_password"]
        password = request.POST["password"]
        confirmation = request.POST["confirmation"]
        if password != confirmation:
            return render(
                request,
                "registration/change_password.html",
                {"message": "Passwords must match."},
            )
        # confirm if the old passwords match
        authentication = authenticate(
            request, username=request.user.username, password=old_password
        )
        if not authentication:
            return render(
                request,
                "registration/change_password.html",
                {"message": "Old password doesn't match."},
            )
        else:
            user.set_password(password)
            user.save()
            login(request, user)
            return HttpResponseRedirect(reverse("setting"))
    return render(request, "registration/change_password.html")


def delete_account(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    if request.method == "POST":
        user = User.objects.get(pk=request.user.pk)
        user.delete()
        return HttpResponseRedirect(reverse("account"))
    return render(request, "delete_account.html")


# Products functions
def create_item(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("login"))
    if request.method == "POST":
        form = AddProdForm(request.POST or None)
        item = Product.objects.filter(user=request.user.id)
        if form.is_valid():
            for it in item:
                if it.url == form.cleaned_data["url"]:
                    request.session["message"] = True
                    return redirect("create_item")
            link = form.cleaned_data["url"]
            name, price = get_link_data(link)
            items = Product.objects.create(
                user=request.user,
                name=name,
                url=link,
                desired_price=form.cleaned_data["desired_price"],
                web_price=price,
            )
            items.save()
            return redirect("create_item")
    form = AddProdForm()
    qs = Product.objects.filter(user=request.user.id)
    items_no = qs.count()
    mess = ""
    if request.session.get("message"):
        mess = True
        del request.session["message"]
    print(mess)
    context = {
        "qs": qs,
        "items_no": items_no,
        "form": form,
        "mesa": mess,
    }
    return render(request, "create_item.html", context)


class ItemUpdateViewNoUrl(UpdateView):
    model = Product
    template_name = "edit_item.html"
    fields = ["url", "name", 'desired_price']
    succes_url = reverse_lazy("create_item")

    def post(self, request, *args, **kwargs):
        keys = list(request.POST.keys())
        prod = Product.objects.filter(url=keys[-1]).first()
        name, price = get_link_data(prod.url)
        prod.web_price = price
        prod.name = name
        prod.desired_price = request.POST["desired_price"]
        prod.save()
        return redirect("create_item")


class ItemDeleteView(DeleteView):
    model = Product
    template_name = "delete_item.html"
    context_object_name = "context_d"
    fields = "__all__"
    success_url = reverse_lazy("create_item")
